package ru.t1.chubarov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role)" +
            " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{locked}, #{firstName}, #{lastName}, #{middleName}, #{role})")
    void add(@NotNull final User user);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password")
            , @Result(property = "firstName", column = "first_name")
            , @Result(property = "lastName", column = "last_name")
            , @Result(property = "middleName", column = "middle_name")
    })
    User findByLogin(@NotNull @Param("login") String login) throws Exception;

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password")
            , @Result(property = "firstName", column = "first_name")
            , @Result(property = "lastName", column = "last_name")
            , @Result(property = "middleName", column = "middle_name")
    })
    User findByEmail(@NotNull String email) throws Exception;

    @Nullable
    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "passwordHash", column = "password")
            , @Result(property = "firstName", column = "first_name")
            , @Result(property = "lastName", column = "last_name")
            , @Result(property = "middleName", column = "middle_name")
    })
    User findOneById(@NotNull String id) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_user WHERE login = #{login}")
    int isLoginExist(@NotNull String login) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_user WHERE email = #{email}")
    int isEmailExist(@NotNull String email) throws Exception;

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, middle_name = #{middleName} WHERE id = #{id}")
    void update(@NotNull User user) throws Exception;

    @Delete("DELETE FROM tm_user;")
    void clear();

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull final User model) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize() throws Exception;

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password")
            , @Result(property = "firstName", column = "first_name")
            , @Result(property = "lastName", column = "last_name")
            , @Result(property = "middleName", column = "middle_name")
    })
    List<User> findAll() throws Exception;

}
