package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.model.IWBS;
import ru.t1.chubarov.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
@Table(name = "tm_project")
public final class Project extends AbstractUserOwnerModel implements IWBS {

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @NotNull
    @Column(nullable = false, name = "description")
    private String description = "";

    @NotNull
    @Column(nullable = false, name = "status")
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    public Project(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

}
