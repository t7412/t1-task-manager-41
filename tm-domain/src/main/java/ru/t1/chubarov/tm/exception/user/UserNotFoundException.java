package ru.t1.chubarov.tm.exception.user;

import ru.t1.chubarov.tm.exception.field.AbstractFieldException;

public final class UserNotFoundException extends AbstractFieldException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
